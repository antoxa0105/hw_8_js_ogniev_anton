//Обработчик события это функция, которая запускается когда наступает определенное событие (клик. фокус и т.д)

myList.classList.add("container");

input.addEventListener('blur', function(){
    if (input.value < 0 || input.value == ""){
        document.querySelector('.label').insertAdjacentHTML('afterend', `<span class= "errorMesage">Please enter correct price</span>`);        
        input.classList.add('errorEnter');
    } else {
        input.classList.remove('errorEnter');
        this.classList.add("input-done");
        myList.insertAdjacentHTML('afterBegin', `<span class= "price">Текущая цена: ${this.value} грн <button class ="remove">x</button></span>`);
            document.querySelector('.remove').addEventListener('click', function(){
            this.parentNode.remove();
            input.value = null;
        });
    };
    input.classList.remove('inputfocus'); 
});

input.addEventListener('focus', function () {
   if (document.querySelector('.errorMesage') != null) {
    document.querySelector('.errorMesage').remove();
    };
    input.classList.add('inputfocus');
});


